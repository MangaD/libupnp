# libupnp

[![pipeline status](https://gitlab.com/MangaD/libupnp/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/MangaD/libupnp/commits/master) [![Build status](xxx)](https://ci.appveyor.com/project/MangaD/libupnp) [![coverage report](https://gitlab.com/MangaD/libupnp/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/MangaD/libupnp/commits/master) [![license](https://img.shields.io/badge/license-MIT-red?style=flat-square)](LICENSE)

libupnp is a C++11 library for forwarding ports in UPnP enabled routers.

## Build

See [documentation/BUILD.md](documentation/BUILD.md).

## Methodology

See [documentation/METHODOLOGY.md](documentation/METHODOLOGY.md).

## Contributing

See [documentation/CONTRIBUTING.md](documentation/CONTRIBUTING.md).

## Documentation

Documentation is generated using Doxygen (and doxywizard). Needs code review.

## External Libraries

- [libsocket](https://bitbucket.org/MangaD/libsocket) by MangaD.
- [Catch2](https://github.com/catchorg/Catch2) test framework.

## Credits

Project badge images created with https://shields.io/.
