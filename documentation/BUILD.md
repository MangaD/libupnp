# Build

## Get source

`git clone --recursive -j4 https://gitlab.com/MangaD/libupnp.git`

## Using GNU Make (Windows, Linux, Mac)

### Install build tools

#### Linux

**Debian/Ubuntu/Mint:** `$ sudo apt-get install build-essential g++ g++-multilib`

**RedHat/CentOS/Fedora:** `$ sudo yum install gcc gcc-c++ glibc-devel libgcc libstdc++-devel
glibc-devel.i686 libgcc.i686 libstdc++-devel.i686`

**Arch:** `$ sudo pacman -S base-devel`

#### Windows

##### MinGW Compiler

* Download latest [MinGW](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/)

*   Install MinGW
    *   Architecture: i686 - for compiling 32 bit programs
    *   Architecture: x86_64 - for compiling 64 bit programs
    *   Threads: posix
    
* Copy "mingw32-make.exe" and rename the copy to "make.exe"

* Put the MinGW bin folder in the path

### Build

Use `$ make debug` for debug and `$ make release` for release. If compiling for a specific architecture use:

##### Debug 32-bit
`$ make debug32`

##### Debug 64-bit
`$ make debug64`

##### Release 32-bit
`$ make release32`

##### Release 64-bit
`$ make release64`

Note: It is possible to compile with Clang by setting CXX environment variable to clang++. `$ CXX=clang++ make`

## Using cmake (Windows, Linux and Mac)

*CMake 2.8.12 or higher is required.*

### Install build tools

Debian/Ubuntu/Mint: `$ sudo apt-get install build-essential g++ g++-multilib cmake`

RedHat/CentOS/Fedora: `$ sudo yum install gcc gcc-c++ glibc-devel libgcc libstdc++-devel
glibc-devel.i686 libgcc.i686 libstdc++-devel.i686 cmake`

Arch: `$ sudo pacman -S base-devel cmake`

Windows: Download [Cmake](https://cmake.org/download/). You need either [MinGW](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/) or [Visual Studio](https://www.visualstudio.com/).

### Build

```sh
$ mkdir build
$ cd build
```

Use `-DCMAKE_BUILD_TYPE:STRING=Debug` for debug, `-DCMAKE_BUILD_TYPE:STRING=Release` for release and `-DCMAKE_BUILD_TYPE:STRING=Coverage` for coverage.

Generate Unix Makefile `$ cmake ..`

Generate Unix CodeBlocks project: `$ cmake -G "CodeBlocks - Unix Makefiles" ..`

Generate MinGW Makefile: `$ cmake -G "MinGW Makefiles" ..`

Generate MinGW CodeBlocks project: `$ cmake -G "CodeBlocks - MinGW Makefiles" ..`

Generate Visual Studio 2015 project: `$ cmake -G "Visual Studio 14 2015" ..`

Generate Visual Studio 2015 project (64-bit): `$ cmake -G "Visual Studio 14 2015 Win64" ..`

...
