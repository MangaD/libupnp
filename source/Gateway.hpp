#ifndef Gateway102019
#define Gateway102019

#include <string>

class Gateway {
public:
	bool openPort(int port, bool udp);
	bool closePort(int port, bool udp);
	bool isMapped(int port, bool udp);
	std::string getExternalIP();
	std::string getLocalIP();
};

#endif
