#include "GatewayFinder.hpp"

const std::array<std::string, 3> GatewayFinder::searchMessages {{
	{ std::string(searchHeader) + "urn:schemas-upnp-org:device:InternetGatewayDevice:1" + searchFooter },
	{ std::string(searchHeader) + "urn:schemas-upnp-org:service:WANIPConnection:1" + searchFooter },
	{ std::string(searchHeader) + "urn:schemas-upnp-org:service:WANPPPConnection:1" + searchFooter }
}};
