#ifndef GatewayFinder102019
#define GatewayFinder102019

#include <array>
#include <string>

class GatewayFinder {
public:
	inline bool isSearching() {
		// TODO
		return false;
	}

private:
	constexpr static char const *searchHeader = "M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nST: ";
	constexpr static char const *searchFooter = "\r\nMAN: \"ssdp:discover\"\r\nMX: 2\r\n\r\n";
	const static std::array<std::string, 3> searchMessages;
};

#endif
