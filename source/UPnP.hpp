#ifndef UPnP102019
#define UPnP102019

#include <memory>
#include <chrono>
#include <thread>
#include <string>

#include "Gateway.hpp"
#include "GatewayFinder.hpp"

class UPnP {
public:
	/**
	 * Waits for UPnP to be initialized (takes ~3 seconds).<br>
	 * It is not necessary to call this method manually before using UPnP functions
	 */
	inline static void waitInit() {
		while (finder.isSearching()) {
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
	}

	/**
	 * Is there an UPnP gateway?
	 * This method is blocking if UPnP is still initializing
	 * All UPnP commands will fail if UPnP is not available
	 *
	 * @return true if available, false if not
	 */
	inline static bool isUPnPAvailable(){
		waitInit();
		return defaultGW != nullptr;
	}

	/**
	 * Opens a TCP port on the gateway
	 *
	 * @param port TCP port (0-65535)
	 * @return true if the operation was successful, false otherwise
	 */
	inline static bool openPortTCP(int port) {
		if(!isUPnPAvailable()) return false;
		return defaultGW->openPort(port, false);
	}

	/**
	 * Opens a UDP port on the gateway
	 *
	 * @param port UDP port (0-65535)
	 * @return true if the operation was successful, false otherwise
	 */
	inline static bool openPortUDP(int port) {
		if(!isUPnPAvailable()) return false;
		return defaultGW->openPort(port, true);
	}

	/**
	 * Closes a TCP port on the gateway<br>
	 * Most gateways seem to refuse to do this
	 *
	 * @param port TCP port (0-65535)
	 * @return true if the operation was successful, false otherwise
	 */
	inline static bool closePortTCP(int port) {
		if(!isUPnPAvailable()) return false;
		return defaultGW->closePort(port, false);
	}

	/**
	 * Closes a UDP port on the gateway<br>
	 * Most gateways seem to refuse to do this
	 *
	 * @param port UDP port (0-65535)
	 * @return true if the operation was successful, false otherwise
	 */
	inline static bool closePortUDP(int port) {
		if(!isUPnPAvailable()) return false;
		return defaultGW->closePort(port, true);
	}

	/**
	 * Checks if a TCP port is mapped<br>
	 *
	 * @param port TCP port (0-65535)
	 * @return true if the port is mapped, false otherwise
	 */
	inline static bool isMappedTCP(int port) {
		if(!isUPnPAvailable()) return false;
		return defaultGW->isMapped(port, false);
	}

	/**
	 * Checks if a UDP port is mapped<br>
	 *
	 * @param port UDP port (0-65535)
	 * @return true if the port is mapped, false otherwise
	 */
	inline static bool isMappedUDP(int port) {
		if(!isUPnPAvailable()) return false;
		return defaultGW->isMapped(port, false);
	}

	/**
	 * Gets the external IP address of the default gateway
	 *
	 * @return external IP address as string, or empty string if not available
	 */
	inline static std::string getExternalIP(){
		if(!isUPnPAvailable()) return "";
		return defaultGW->getExternalIP();
	}

	/**
	 * Gets the internal IP address of this machine
	 *
	 * @return internal IP address as string, or empty string if not available
	 */
	inline static std::string getLocalIP(){
		if(!isUPnPAvailable()) return "";
		return defaultGW->getLocalIP();
	}

private:
	static std::unique_ptr<Gateway> defaultGW;
	static GatewayFinder finder;
};

#endif
